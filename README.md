# Nova

**NOVA**

* Programs
    * - [x] Fhvae
    * - [x] Deep Markov Model (DMM)
    * - [x] Fhvae GMM
    * - [ ] DMMSeg (infer Segmentation)
* Latent Space Analysis
    * - [x] TSNE
    * - [x] Clustering (Non-Parametric)
    * - [x] Spectrogram Reconstructions
    * - [ ] Waveform Reconstruction


Searching for phone and speaker clusters in the latent space and being able to reconstruct spectrograms from the latent space, all seem fine. But, staying true to the original goal of language acquisition, following question comes to mind:

* When can i say that the program has acquired language understanding?
    * One shot learning. Can the program learn a new concept from just one example of that concept. For e.g. If we build an Enligh Acquisition program, can it learn Arabic? Does the learning behaviour follows the pattern of a human English speaker?
        * Lake, Brenden M., Ruslan Salakhutdinov, and Joshua B. Tenenbaum. "Human-level concept learning through probabilistic program induction." Science 350.6266 (2015): 1332-1338.
        * Lake, Brenden, et al. "One-shot learning of generative speech concepts." Proceedings of the Annual Meeting of the Cognitive Science Society. Vol. 36. No. 36. 2014.
    * Program is able to generate speech (Analysis by synthesis)? 
    * Program's latent structure points to some well known linguistic phenomenon?
    * Program is able to learn rules of phonology?
    * Given speech, program is able to imagine a scene that appropriately describes what is being said?
    * Given speech, program is able to perform some task?

![](http://people.csail.mit.edu/sameerk/recons/brain)

<!--*A probabilistic programming framework*-->
<!--* Reconstructions for Fhvae, DMM, Fhvae GMM.-->

<!--Model | FBANK             |  SPECTROGRAM-->
<!--:-------------------------:|:-------------------------:|:-------------------------:-->
<!--![](nova/fhvae_lstm.png) **256 DIM** | ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_fhvae_550.png)  |  ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_fhvae_750.png)-->
<!--![](nova/dmm.png) **DMM**| ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_dmm_seq6_epoch750.png)  |  ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_dmm_seq3_epoch750.png)-->
<!--![](nova/disc.png) **Fhvae GMM** | ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_fhvae_gmm_200.png)  |  ![](http://people.csail.mit.edu/sameerk/recons/recon_lstm_fhvae_gmm_150.png)-->


<!--## Neural EWER :fire:-->

<!--* Plots we saw on wednesday-->

<!--Model | Feats             |  EWER-->
<!--:-------------------------:|:-------------------------:|:-------------------------:-->
<!--LSTM-32 | Alien Phone Sequence  |  ![](http://people.csail.mit.edu/sameerk/recons/wer_lstm_32_alien_phn.png)-->
<!--LSTM-32 | Decoded Word Sequence  |  ![](http://people.csail.mit.edu/sameerk/recons/wer_lstm_wrds_32.png)-->
<!--LSTM-32 | Decoded Grapheme Sequence  |  ![](http://people.csail.mit.edu/sameerk/recons/wer_lstm_32_phn.png)-->
<!--FeedFor | Decoder Features  |  ![](http://people.csail.mit.edu/sameerk/recons/wer.png)-->
<!--Model | Acoustic Features | **WIP**-->

<!--## Semi-Sup Speech Transcription-->

<!--* conference: **ICASSP**, Nov. 2018-->

<!--* collaborators: Ahmed-->

<!--* Resuls-->

<!--### acoustic modeling-->

<!--| Model  | Arch | Data-sup | Data-unsup | LM | wer-dev | wer-test |-->
<!--| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |-->
<!--| tdnn  | tdnnx6 | 50k | - | 4-gram | 26 | - |-->
<!--| tdnn-deep  | (tdnn+linx2+tdnn+lin)x2+(tdnn+linx2)x6+tdnn+lin | 50k | - | 4-gram | 22.8 | - |-->
<!--| tdnn-deep-att  | " + att | 50k | - | 4-gram | 22.9 | - |-->
<!--| tdnn-lstm  | tdnnx3+lstm+(tdnnx2+lstm)x2 | 50k | - | 4gram | 23.12 | - |-->
<!--| tdnn-lstm-deep  | (tdnn+lin)x6+(lstm+(tdnn+lin)x2)x2 | 50k | - | 4gram | **22.11** | - |-->
<!--| tdnn-Blstm  | tdnnx3+blstmx3 | 50k | - | 4gram | 23.84 | - |-->
<!--| tdnn-Blstm-deep | (tdnn+2xlin+tdnn+lin)x2+(tdnn+2xlin)x6+tdnn+blstmx3 | 50k | - | 4gram | 23.71 | - |-->
<!--| tdnn  | - | 50k | 250k | 4-gram | 23.11 | - |-->
<!--| tdnn-lstm-deep  | - | 50k | 250k | 4gram | 20.2 | - |-->
<!--| tdnn-Blstm-deep | - | 50k | 250k | 4gram | **WIP** | - |-->

<!--### language modeling-->

<!--| LM  | Vocab | Train-dat | Test-dat | PPL | OOV |-->
<!--| ------------- | ------------- | ------------- | ------------- | ------------- | ------------- |-->
<!--| 3-gram  | all | mgb2-transcripts | mgb2-dev  | 1063.3 | 1928 |-->
<!--| word-lstm256-hsoftmax  | all | mgb2-transcripts  | mgb2-dev  | 2393.5 | 1928 |-->
<!--| 3-gram  | 40k | mgb2-transcripts | mgb2-dev  | 739.5 | 5709 |-->
<!--| word-blstm256-softmax  | 40k | mgb2-transcripts  | mgb2-dev | 201.3 | 5709 |-->
<!--| gated-cnn | 40k | mgb2-transcripts | mgb2-dev | 473.8| 5709 |-->


<!--* To Dos-->

<!--- [ ] Deep Language Modeling (Char)-->


<!--* **Next Steps**-->
<!--    * Hand it over to Ahmed to run on MGB3. Won't be able to do much till May 22nd-->


<!--## Supervised Speech Transcription (No Progress)-->
<!--* Ahmed Continues to run kaldi scripts on MGB2-->

<!--* Collaborators : Ahmed-->

<!--* prepare manuscript for possible CSL Journal Submission-->

<!--## Interpreting Neural Nets (No Progress)-->

<!--* conference : **ICASSP**, Nov. 2018-->

<!--* collaborators : ?-->

<!--## Misc-->

<!--### Bioinformatics-->

<!--* worked on a problem when I was in QCRI-->

<!--* got published in the premier bioinformatics journal (Journal of Bioinformatics (**Impact 7.3**)) recently-->
<!--    *  **Khurana, Sameer**, et al. "DeepSol: A Deep Learning Framework for Sequence-Based Protein Solubility Prediction." Bioinformatics 1 (2018): 9. [Paper] (https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/bty166/4938490?guestAccessKey=503bcad3-2de1-4cec-a97c-00a3c6e16263)-->

<!--* We are thinking about extending the work and submit in **NIPS** -->